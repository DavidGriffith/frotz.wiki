Frotz: A Portable Z-Machine Interpreter
=======================================

Frotz is an interpreter for Infocom games and other Z-machine games. It complies with standard 1.0 of Graham Nelson's specification. It was written by Stefan Jokisch in 1995-1997. It was ported to Unix by Galen Hazelwood. Currently the Unix port is being developed and maintained by David Griffith. The Unix port is also the canonical core version upon which all other ports are based.

* Compiles and runs without changes on most common flavors of Unix, both open source and not. This includes MacOS X.
* Core code widely portable to lots of different platforms.
* Plays all Z-code games including V6.
* Old-style sound support through OSS driver.
* Config files.
* User-configurable error-checking.
* Optional dumb interface.
* Distributed under the GNU General Public License.
* Packages available for all popular distributions of Linux and BSD. 

Credits
=======

This program started as a remake of Mark Howell's Zip, but has grown into a completely new interpreter with ports for lots of platforms. 

Thanks goes to Stefan Jokisch for writing Frotz and Galen Hazelwood for doing the initial work of porting it to Unix. Thanks also to Jim Dunleavy for his optimization to the core Frotz code. Other people to thank include those who uploaded patches to Unix Frotz to the IF-Archive and getting me interested in the inner-workings of interpreters like Frotz, people who posted to Usenet feedback on what I was doing, testers, those who donated the use of machines for porting, sent me patches, etc. These include, but are not limited to: 

Torbjorn Anderson, Timo Korvola, Martin Frost, Mihail Milushev, David Picton, Chris Sullivan, Leonard Richardson, Stephen Kitt, Paul E Coad, Paul Janzen, Brad Town, Jason C Penney, Denis Hirschfeldt, Jacob Nevins, Matteo De Luigi, Steven Frank, Thomas Troeger, and others that I've forgotten. 

Michael Edmonson (author of Rezrov) and Evin Robertson (author of Nitfol) deserve recognition for the ideas that I've borrowed from their programs. Al Petrofsky (creator of dumbfrotz) donated much code to allow Unix Frotz to semi-support V6 games. 
