[Unix Frotz 2.44 source code package](http://ifarchive.org/if-archive/infocom/interpreters/frotz/frotz-2.44.tar.gz) (for all flavors of Unix including Mac OS X)

[Windows Frotz installer](http://ifarchive.org/if-archive/infocom/interpreters/frotz/WindowsFrotzInstaller.exe)

[iFrotz](https://itunes.apple.com/us/app/frotz/id287653015) (for iPhone, iPod, and iPad)